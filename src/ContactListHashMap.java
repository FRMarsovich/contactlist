import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ContactListHashMap {
    public static HashMap<String, Integer> contactList = new HashMap<>();

    public static void main(String[] args) throws IOException {
        ContactListHashMap list = new ContactListHashMap();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String choose = "";
        while (true) {
            System.out.println("1 - add contact");
            System.out.println("2 - view all contacts");
            System.out.println("3 - delete contact");
            System.out.println("4 - show phone number: ");
            System.out.println("5 - quit");
            System.out.println("Choose: ");

            try {
                choose = bufferedReader.readLine();
            } catch (IOException e) {
                System.out.println("Error");
            }

            switch (choose) {
                case "1":
                    list.addContact();
                    break;
                case "2":
                    list.viewContacts();
                    break;
                case "3":
                    list.deleteContact();
                    break;
                case "4":
                    list.showContact();
                    break;
                case "5":
                    System.exit(0);
                    break;
                default:
                    System.out.println("Error");
                    break;
            }
        }
    }

    public void addContact() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Name: ");
        String name = reader.readLine();
        System.out.println("Phone: ");
        Integer phone = Integer.parseInt(reader.readLine());

        contactList.put(name, phone);
    }

    public void viewContacts() {
        Iterator<Map.Entry<String, Integer>> iterator = contactList.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> pair = iterator.next();
            String key = pair.getKey();
            Integer value = pair.getValue();
            System.out.println(key + ": " + value);
        }
    }

    public void deleteContact() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Write name for delete contact: ");
        String keyDelete = reader.readLine();
        for (Iterator<Map.Entry<String, Integer>> iterator = contactList.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, Integer> entry = iterator.next();
            if(entry.getKey().equals(keyDelete)) {
                iterator.remove();
            }
        }

    }

    public void showContact() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Write name for show phone number");
        String name = reader.readLine();
        for (Iterator<Map.Entry<String, Integer>> iterator = contactList.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, Integer> entry = iterator.next();
            if (entry.getKey().equals(name)) {
                System.out.println(entry.getValue());
            }
        }
    }
}
